# frozen_string_literal: true

require_relative 'lib/codebreaker/version'

Gem::Specification.new do |s|
  s.name          = 'codebreaker'
  s.version       = Codebreaker::VERSION
  s.license       = 'GPL-3.0-or-later'
  s.authors       = ['emerac']
  s.email         = ['7785766-emerac@users.noreply.gitlab.com']
  s.homepage      = 'https://gitlab.com/emerac/codebreaker-game'
  s.summary       = 'A game where the player must guess the secret code.'
  s.description   = <<~DESC
    A single-player, console-based game where the player is tasked with
    cracking a secret code. After each attempt, the player is given
    feedback, which can be used to make successive guesses.
  DESC
  s.required_ruby_version = '>= 2.7.4'

  s.metadata = {
    'allowed_push_host' => 'https://rubygems.org',
    'homepage_uri' => s.homepage,
    'source_code_uri' => s.homepage
  }

  s.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{\A(?:test|spec|features)/})
    end
  end
  s.bindir        = 'exe'
  s.executables   = s.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  s.require_paths = ['lib']

  s.add_development_dependency 'rake', '~> 13.0'
  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'rubocop', '~> 1.22'
  s.add_development_dependency 'rubocop-rake', '~> 0.0'
  s.add_development_dependency 'rubocop-rspec', '~> 2.6'
  s.add_development_dependency 'simplecov', '~> 0.20'
end
