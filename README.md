# codebreaker game

[![pipeline status](https://gitlab.com/emerac/codebreaker-game/badges/master/pipeline.svg)](https://gitlab.com/emerac/codebreaker-game/-/commits/master)
[![coverage report](https://gitlab.com/emerac/codebreaker-game/badges/master/coverage.svg)](https://gitlab.com/emerac/codebreaker-game/-/commits/master)

A single-player, console-based game where the player is tasked with
cracking a secret code. After each attempt, the player is given
feedback, which can be used to make successive guesses.

## Install

This program requires Ruby 2.7.4 or higher and
[bundler](https://bundler.io/). If you don't have bundler already,
`gem install bundler` will install it for you.

To install this game:
1. Clone this repository to a convenient location.
2. Change directory into the newly cloned repository.
3. Run the `bin/setup` script.

## Usage

After the setup script completes, run `bin/play` to give the game a go!

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](./LICENSE) file.
