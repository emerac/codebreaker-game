# 0.2.0 (2021-11-18)

* feat: add a pre-made object and require stringio (987ab43)
* test: add tests for the Game class (b1d9205)
* refactor: simplify modules and improve testability (a0b15e4)
* refactor: add SimpleCov test coverage tracking (8a6e5ca)
* test: add tests for the Board class (bd24b9f)

# 0.1.0 (2021-11-17)

* initial release
