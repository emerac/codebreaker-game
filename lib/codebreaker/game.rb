# frozen_string_literal: true

# Provide the ability to play the codebreaker game.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Codebreaker
  # Represent the state of a codebreaker game.
  class Game
    # Creates the game from a hash that contains setup arguments.
    #
    # If no setup hash is provided, a default game will be created.
    attr_reader :board, :number_of_valid_digits, :valid_digits
    attr_accessor :is_human_codebreaker

    def initialize(setup = {})
      @board = setup.fetch(:board, Board.new)
      @sequence_amount = @board.sequence_amount
      @sequence_width = @board.sequence_width
      @number_of_valid_digits = setup.fetch(:number_of_valid_digits, 6)
      @valid_digits = setup.fetch(:valid_digits, default_valid_digits)
      @is_human_codebreaker = true
      @is_game_over = false
      @input = setup.fetch(:input, $stdin)
      @output = setup.fetch(:output, $stdout)
    end

    def ask_human_for_code
      @output.print code_solicitation
      code = obtain_code_from_human
      until valid_code?(code)
        @output.puts code_error_message
        @output.print code_solicitation
        code = obtain_code_from_human
      end
      code
    end

    def ask_if_human_codebreaker
      @output.print role_solicitation
      choice = obtain_choice
      until valid_choice?(choice)
        @output.puts choice_error_message
        @output.print role_solicitation
        choice = obtain_choice
      end
      choice
    end

    def can_human_guess_the_code?
      winner = false
      @sequence_amount.downto(1) do |i|
        @output.puts
        @board.display_board(false, @output)
        @output.puts sequences_remaining(i)
        code = ask_human_for_code
        @board.guesses[i - 1] = code
        @board.feedback[i - 1] = obtain_feedback(code)

        if code == @board.master_code
          winner = true
          break
        end
      end
      @board.display_board(true, @output)
      winner
    end

    def can_computer_guess_the_code?(time = 1)
      winner = false
      @sequence_amount.downto(1) do |i|
        @output.puts
        sleep time unless i == @sequence_amount
        @board.display_board(true, @output)
        @output.puts sequences_remaining(i)
        code = obtain_computer_code(i)
        @output.puts computer_guess_message(code)
        @board.guesses[i - 1] = code
        @board.feedback[i - 1] = obtain_feedback(code)

        if code == @board.master_code
          winner = true
          break
        end
      end
      @board.display_board(true, @output)
      winner
    end

    def determine_master_code
      if @is_human_codebreaker
        @board.master_code = generate_code
      else
        @board.master_code = ask_human_for_code
      end
    end

    def end_game(is_human_winner)
      @output.puts game_over_message(is_human_winner)
      @output.puts computer_end_message(is_human_winner)
    end

    def fix_roles(choice)
      @is_human_codebreaker = %w[yes y].include?(choice)
    end

    def introduce_game
      @output.puts license
      @output.puts
      @output.puts instructions
      @output.puts
    end

    def perform_setup
      introduce_game
      choice = ask_if_human_codebreaker
      fix_roles(choice)
      determine_master_code
    end

    def play
      perform_setup
      if @is_human_codebreaker
        is_human_winner = can_human_guess_the_code?
      else
        is_human_winner = !can_computer_guess_the_code?
      end
      end_game(is_human_winner)
    end

    private

    def choice_error_message
      'Error: an invalid choice was entered.'
    end

    def generate_code
      Array.new(@sequence_width) { @valid_digits.sample }
    end

    def code_error_message
      w = @sequence_width
      max = @valid_digits.max
      min = @valid_digits.min
      <<~MSG
        Error: an invalid code was entered.
        A valid code is a #{w}-digit number containing only digits
        between #{min} and #{max}, inclusive.
      MSG
    end

    def code_solicitation
      "Enter a code:\n> "
    end

    def computer_end_message(is_human_winner)
      if is_human_winner
        'Computer: \'Congratulations, human.\''
      else
        'Computer: \'Silly human! I am the code master!\''
      end
    end

    def computer_guess_message(code)
      "Computer: \'I guess: #{code.join}\'"
    end

    def default_valid_digits
      arr = Array.new(@sequence_width)
      @number_of_valid_digits.times { |i| arr[i] = (i + 1) }
      arr
    end

    def game_over_message(is_human_winner)
      is_human_winner ? 'You won!' : 'You lost!'
    end

    def generate_guess(last_guess)
      guess = Array.new(@sequence_width)
      last_guess.each_with_index do |digit, i|
        if digit == @board.master_code[i]
          guess[i] = last_guess[i]
        else
          guess[i] = @valid_digits.sample
        end
      end
      guess
    end

    def instructions
      a = @sequence_amount
      w = @sequence_width
      digits = @valid_digits.join(' ')
      <<~INSTRUCTIONS
        Welcome to codebreaker!

        Introduction:
        The goal is to guess the master code.  The codemaker will create
        a #{w}-digit master code containing these digits: #{digits}. The
        codebreaker must guess the master code within #{a} turns.  After
        each guess, the codebreaker is given feedback about the guess
        they just made.

        Feedback:
        - the ordering of the each feedback sequence is random
        - an 'X' is given for every correct digit in the correct position
        - an 'O' is given for every correct digit in the wrong position
        - if the guess contains duplicate digits, no feedback will be given
          unless the master code contains the same number of duplicates
      INSTRUCTIONS
    end

    def license
      <<~LICENSE
        codebreaker-game Copyright (C) 2021 emerac
        This program comes with ABSOLUTELY NO WARRANTY; This is free
        software, and you are welcome to redistribute it under certain
        conditions; You should have received a copy of the GNU General
        Public License along with this program.  If not, see
        <https://www.gnu.org/licenses/>.
      LICENSE
    end

    def obtain_choice
      @input.gets.chomp.downcase
    end

    def obtain_code_from_human
      Integer(@input.gets).digits.reverse
    rescue ArgumentError
      [-1, -1, -1, -1]
    end

    def obtain_computer_code(sequences_remaining)
      if sequences_remaining == @sequence_amount
        code = generate_code
      else
        code = generate_guess(@board.guesses[sequences_remaining])
      end
      code
    end

    def obtain_feedback(code)
      m = @board.master_code
      feedback = Array.new(@sequence_width)
      code.each_with_index do |digit, i|
        if digit == m[i]
          feedback[i] = 'X'
        elsif m.include?(digit) && (code.count(digit) <= m.count(digit))
          feedback[i] = 'O'
        else
          feedback[i] = '-'
        end
      end
      feedback.shuffle
    end

    def role_solicitation
      "Would you like to be the codebreaker (y/n)?\n> "
    end

    def sequences_remaining(sequences_remaining)
      "Turns remaining: #{sequences_remaining}"
    end

    def valid_choice?(choice)
      %w[yes y no n].include?(choice)
    end

    def valid_code?(code)
      return false if code.length != @sequence_width

      code.each { |digit| return false unless @valid_digits.include?(digit) }
      true
    end
  end
end
