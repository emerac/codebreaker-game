# frozen_string_literal: true

# Provide the board used to play codebreaker.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Codebreaker
  # Represent the state of a codebreaker board.
  class Board
    attr_reader :sequence_amount, :sequence_width
    attr_accessor :guesses, :feedback, :master_code

    # Creates the board from a hash that contains setup arguments.
    #
    # If no setup hash is provided, a default board will be created.
    def initialize(setup = {})
      @sequence_amount = setup.fetch(:sequence_amount, 12)
      @sequence_width = setup.fetch(:sequence_width, 4)
      @feedback = grid(@sequence_amount, @sequence_width)
      @guesses = grid(@sequence_amount, @sequence_width)
      @empty_sequence = empty_sequence
      @hidden_sequence = hidden_sequence
      @master_code = setup.fetch(:master_code, Array.new(@sequence_width))
    end

    def display_board(is_master_code_visible, output = $stdout)
      output.puts margin + horizontal_border
      output.puts row(@master_code, @empty_sequence, is_master_code_visible)
      output.puts margin + horizontal_border
      @sequence_amount.times do |i|
        row_guess = @guesses[i].all?(nil) ? @empty_sequence : @guesses[i]
        row_feedback = @feedback[i].all?(nil) ? @empty_sequence : @feedback[i]
        output.puts row(row_guess, row_feedback, true)
      end
      output.puts margin + horizontal_border
    end

    private

    def empty_sequence
      arr = Array.new(@sequence_width)
      arr.each_index { |i| arr[i] = '-' }
    end

    def grid(height, width)
      Array.new(height) { Array.new(width) }
    end

    def hidden_sequence
      arr = Array.new(@sequence_width)
      arr.each_index { |i| arr[i] = '?' }
    end

    def horizontal_border
      border_char = '─'
      border_size = 19
      " #{border_char * border_size}"
    end

    def margin
      margin_char = ' '
      margin_size = 10
      margin_char * margin_size
    end

    def row(code, feedback, is_code_visible)
      left_side = is_code_visible ? code.join(' ') : @hidden_sequence.join(' ')
      right_side = feedback.join(' ')
      "#{margin}│ #{left_side} │ #{right_side} │"
    end
  end
end
