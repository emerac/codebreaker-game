# frozen_string_literal: true

require_relative 'codebreaker/board'
require_relative 'codebreaker/game'
require_relative 'codebreaker/version'

# Namespace module. Contains all codebreaker classes and constants.
module Codebreaker
end
