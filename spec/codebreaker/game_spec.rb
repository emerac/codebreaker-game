# frozen_string_literal: true

# Tests for Codebreaker::Game.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require 'stringio'

require_relative '../spec_helper'

describe Codebreaker::Game do
  let(:board_setup) do
    {
      master_code: [1, 2, 3, 4]
    }
  end

  let(:game_setup) do
    {
      board: Codebreaker::Board.new(board_setup),
      output: StringIO.new
    }
  end

  let(:game) { described_class.new(game_setup) }

  describe '#initialize' do
    context 'when allowing defaults to be used' do
      it 'uses the default sequence amount' do
        expect(game.board.sequence_amount).to eq(12)
      end

      it 'uses the default sequence width' do
        expect(game.board.sequence_width).to eq(4)
      end

      it 'uses the default number of valid digits' do
        expect(game.number_of_valid_digits).to eq(6)
      end

      it 'defaults to using an array to hold the default valid digits' do
        expect(game.valid_digits).to be_an_instance_of(Array)
      end

      it 'uses integers as the default valid digits' do
        expect(game.valid_digits).to all(be_an_instance_of(Integer))
      end
    end

    context 'when providing arguments' do
      let(:board_setup) do
        {
          sequence_amount: 4,
          sequence_width: 2,
          master_code: [0, 0]
        }
      end

      let(:game_setup) do
        {
          board: Codebreaker::Board.new(board_setup),
          number_of_valid_digits: 5,
          valid_digits: %w[1 2 3 4 5]
        }
      end

      let(:game) { described_class.new(game_setup) }

      it 'uses the custom sequence amount' do
        expect(game.board.sequence_amount).to eq(board_setup[:sequence_amount])
      end

      it 'uses the custom sequence width' do
        expect(game.board.sequence_width).to eq(board_setup[:sequence_width])
      end

      it 'uses the custom number of valid digits' do
        number_of_valid_digits = game_setup[:number_of_valid_digits]
        expect(game.number_of_valid_digits).to eq(number_of_valid_digits)
      end

      it 'uses the supplied array to hold the valid digits' do
        expect(game.valid_digits).to be_an_instance_of(Array)
      end

      it 'uses custom characters instead of the default valid digits' do
        expect(game.valid_digits).to all(be_an_instance_of(String))
      end
    end
  end

  describe '#ask_human_for_code' do
    it 'returns a properly formatted code when the input is valid' do
      game_setup[:input] = StringIO.new('1234')
      code = game.ask_human_for_code
      expect(code).to eq([1, 2, 3, 4])
    end

    it 'displays an error if the first code entered is invalid' do
      game_setup[:input] = StringIO.new("abcd\n1234")
      game.ask_human_for_code
      expect(game_setup[:output].string).to include('Error')
    end
  end

  describe '#ask_if_human_codebreaker' do
    it 'returns a properly formatted choice when the input is valid' do
      game_setup[:input] = StringIO.new('y')
      choice = game.ask_if_human_codebreaker
      expect(choice).to eq('y')
    end

    it 'displays an error if the first choice entered is invalid' do
      game_setup[:input] = StringIO.new("a\ny")
      game.ask_if_human_codebreaker
      expect(game_setup[:output].string).to include('Error')
    end
  end

  describe '#can_computer_guess_the_code?' do
    it 'returns false if the computer cannot guess the code' do
      board_setup[:sequence_amount] = 1
      game_setup[:input] = StringIO.new("n\n1234")
      expect(game.can_computer_guess_the_code?(0)).to be(false)
    end

    it 'returns true if the computer can guess the code' do
      board_setup[:sequence_amount] = 500
      game_setup[:input] = StringIO.new("n\n1234")
      expect(game.can_computer_guess_the_code?(0)).to be(true)
    end
  end

  describe '#can_human_guess_the_code?' do
    it 'returns false if the human cannot guess the code' do
      board_setup[:sequence_amount] = 1
      game_setup[:input] = StringIO.new('1111')
      expect(game.can_human_guess_the_code?).to be(false)
    end

    it 'returns true if the human can guess the code' do
      board_setup[:sequence_amount] = 1
      game_setup[:input] = StringIO.new('1234')
      expect(game.can_human_guess_the_code?).to be(true)
    end
  end

  describe '#determine_master_code' do
    it 'generates a random code if the human is the codebreaker' do
      game.is_human_codebreaker = true
      game.determine_master_code
      expect(game.board.master_code).to all(be_instance_of(Integer))
    end

    it 'gets code from the human if the human is not the codebreaker' do
      game_setup[:input] = StringIO.new('1234')
      game.is_human_codebreaker = false
      game.determine_master_code
      expect(game.board.master_code).to eq([1, 2, 3, 4])
    end
  end

  describe '#end_game' do
    let(:game_setup) { {} }

    it 'displays a message that the human won when the human wins' do
      expect { game.end_game(true) }.to output(/.*won.*/m).to_stdout
    end

    it 'displays a message that the human lost when the human loses' do
      expect { game.end_game(false) }.to output(/.*lost.*/m).to_stdout
    end

    it 'has the computer congratulate the human when the human wins' do
      expect { game.end_game(true) }.to output(/.*Congrat.*/m).to_stdout
    end

    it 'has the computer gloat when the human loses' do
      expect { game.end_game(false) }.to output(/.*Silly.*/m).to_stdout
    end
  end

  describe '#fix_roles' do
    it 'sets the human to be the codebreaker when the choice is affirmative' do
      game.fix_roles('y')
      expect(game.is_human_codebreaker).to be(true)
    end

    it 'sets the human to be the codemake when the choice is negative' do
      game.fix_roles('n')
      expect(game.is_human_codebreaker).to be(false)
    end
  end

  describe '#introduce_game' do
    let(:game_setup) { {} }

    it 'displays a message containing license information' do
      expect { game.introduce_game }.to output(/GNU.*License/m).to_stdout
    end

    it 'displays a message containing instructions' do
      expect { game.introduce_game }.to output(/Welcome.*!/m).to_stdout
    end
  end

  describe '#play' do
    it 'outputs a consolation when human is codebreaker and loses' do
      board_setup[:sequence_amount] = 1
      game_setup[:input] = StringIO.new("y\n1234")
      game_setup.delete(:output)
      expect { game.play }.to output(/.*You lost!.*/m).to_stdout
    end

    it 'outputs a congratulations when human is codemaker and wins' do
      board_setup[:sequence_amount] = 1
      game_setup[:input] = StringIO.new("n\n1234")
      game_setup.delete(:output)
      expect { game.play }.to output(/.*You won!.*/m).to_stdout
    end
  end
end
