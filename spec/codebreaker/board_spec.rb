# frozen_string_literal: true

# Tests for Codebreaker::Board.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Codebreaker::Board do
  describe '#initialize' do
    context 'when providing no arguments' do
      let(:board) { described_class.new }

      it 'does not raise an error' do
        expect { board }.not_to raise_error
      end

      it 'uses the default sequence amount' do
        expect(board.sequence_amount).to eq(12)
      end

      it 'uses the default sequence width' do
        expect(board.sequence_width).to eq(4)
      end

      it 'creates the feedback grid with the right outer dimensions' do
        expect(board.feedback.length).to eq(board.sequence_amount)
      end

      it 'creates the feedback grid with the right inner dimensions' do
        expect(board.feedback[0].length).to eq(board.sequence_width)
      end

      it 'creates the guesses grid with the right outer dimensions' do
        expect(board.guesses.length).to eq(board.sequence_amount)
      end

      it 'creates the guesses grid with the right inner dimensions' do
        expect(board.guesses[0].length).to eq(board.sequence_width)
      end

      it 'creates the master code with the right dimensions' do
        expect(board.master_code.length).to eq(board.sequence_width)
      end

      it 'sets the master code to contain invalid values' do
        expect(board.master_code).to all(be_nil)
      end
    end

    context 'when providing arguments' do
      let(:setup) do
        {
          sequence_amount: 4,
          sequence_width: 2,
          master_code: [0, 0]
        }
      end

      let(:board) { described_class.new(setup) }

      it 'does not raise an error' do
        expect { board }.not_to raise_error
      end

      it 'uses the custom sequence amount' do
        expect(board.sequence_amount).to eq(setup[:sequence_amount])
      end

      it 'uses the custom sequence width' do
        expect(board.sequence_width).to eq(setup[:sequence_width])
      end

      it 'creates the feedback grid with the right outer dimensions' do
        expect(board.feedback.length).to eq(setup[:sequence_amount])
      end

      it 'creates the feedback grid with the right inner dimensions' do
        expect(board.feedback[0].length).to eq(setup[:sequence_width])
      end

      it 'creates the guesses grid with the right outer dimensions' do
        expect(board.guesses.length).to eq(setup[:sequence_amount])
      end

      it 'creates the guesses grid with the right inner dimensions' do
        expect(board.guesses[0].length).to eq(setup[:sequence_width])
      end

      it 'creates the master code with the right dimensions' do
        expect(board.master_code.length).to eq(setup[:sequence_width])
      end

      it 'sets the master code to contain the custom values' do
        board.master_code.each_index do |i|
          expect(board.master_code[i]).to eq(setup[:master_code][i])
        end
      end
    end
  end

  describe '#display_board' do
    let(:board) { described_class.new }

    it 'displays board characters and the current master code' do
      expect { board.display_board(true) }.not_to output(/(\?\s*){4}/).to_stdout
    end

    it 'displays board characters and not the current master code' do
      expect { board.display_board(false) }.to output(/(\?\s*){4}/).to_stdout
    end
  end
end
